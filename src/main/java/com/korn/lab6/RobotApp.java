package com.korn.lab6;

public class RobotApp {
    public static void main(String[] args) {
        Robot kapong = new Robot("Kapong", 'K', 1, 0);
        Robot blue = new Robot("Blue", 'B', 10, 10);
        kapong.print();
        kapong.right();
        kapong.print();
        blue.print();

        for (int y = Robot.Min_Y; y <= Robot.Max_Y; y++) {
            for (int x = Robot.Min_X; x < Robot.Max_X; x++) {
                if (kapong.getX() == x && kapong.getY() == y) {
                    System.out.print(kapong.getSymbol());
                } else if (blue.getX() == x && blue.getY() == y) {
                    System.out.print(blue.getSymbol());
                } else {
                    System.out.print("-");
                }
                System.out.print("-");
            }
            System.out.println();
        }
    }
}
