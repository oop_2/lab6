package com.korn.lab6;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        BookBank sakontam = new BookBank("Sakontam", 50.0); // Contructor
        sakontam.print();
        sakontam.deposit(40000);
        sakontam.print();

        BookBank Iheretoo = new BookBank("Iheretoo", 100000.0);
        Iheretoo.print();
        Iheretoo.withdraw(40000.0);
        Iheretoo.print();

    }
}
