package com.korn.lab6;

public class Robot {
    private String name;
    private char symbol;
    private int x;
    private int y;
    public static final int Min_X = 0;
    public static final int Min_Y = 0;
    public static final int Max_X = 20;
    public static final int Max_Y = 20;

    public Robot(String name, char symbol, int x, int y) {
        this.name = name;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
    }

    public Robot(String name, char symbol) {
        this(name, symbol, 0, 0);
    }

    public boolean up() {
        if (y == Min_Y)
            return false;
        y = y - 1;
        return true;
    }

    public boolean up(int step) {
        for (int i = 0; i < step; i++) {
            if (!up()) {
                return false;
            }
        }
        return true;
    }

    public boolean down() {
        if (y == Max_Y)
            return false;
        y = y + 1;
        return true;
    }

    public boolean down(int step) {
        for (int i = 0; i < step; i++) {
            if (!down()) {
                return false;
            }
        }
        return true;
    }

    public boolean left() {
        if (x == Min_X)
            return false;
        x = x - 1;
        return true;
    }

    public boolean left(int step) {
        for (int i = 0; i < step; i++) {
            if (!left()) {
                return false;
            }
        }
        return true;
    }

    public boolean right() {
        if (x == Max_X)
            return false;
        x = x + 1;
        return true;
    }

    public boolean right(int step) {
        for (int i = 0; i < step; i++) {
            if (!right()) {
                return false;
            }
        }
        return true;
    }

    public void print() {
        System.out.println("Robot:" + name + " " + "X:" + x + " " + "Y:" + y);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public char getSymbol() {
        return symbol;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
