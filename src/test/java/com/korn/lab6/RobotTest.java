package com.korn.lab6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldDownOver() {
        Robot robot = new Robot("Robot", 'R', 0, Robot.Max_Y);
        assertEquals(false, robot.down());
        assertEquals(Robot.Max_Y, robot.getY());
    }

    @Test
    public void shouldCreatteRobotSuccess() {
        Robot robot = new Robot("Robot", 'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldDownNegative() {
        Robot robot = new Robot("Robot", 'R', 0, Robot.Min_Y);
        assertEquals(false, robot.up());
        assertEquals(Robot.Min_Y, robot.getY());
    }

    @Test
    public void shouldDownSuccess() {
        Robot robot = new Robot("Robot", 'R', 0, 0);
        assertEquals(true, robot.down());
        assertEquals(1, robot.getY());
    }

    @Test
    public void shouldUpNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(5);
        assertEquals(true, result);
        assertEquals(6, robot.getY());
    }

    @Test
    public void shouldUpNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(11);
        assertEquals(true, result);
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldUpNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(12);
        assertEquals(false, result);
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldDownNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.down(5);
        assertEquals(true, result);
        assertEquals(16, robot.getY());
    }

    @Test
    public void shouldDownNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.down(12);
        assertEquals(false, result);
        assertEquals(20, robot.getY());
    }

    @Test
    public void shouldLeftNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.left(5);
        assertEquals(true, result);
        assertEquals(5, robot.getX());
    }

    @Test
    public void shouldLeftNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.left(12);
        assertEquals(false, result);
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldRightNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.left(5);
        assertEquals(true, result);
        assertEquals(5, robot.getX());
    }

    @Test
    public void shouldRightNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.left(12);
        assertEquals(false, result);
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldrightNegative() {
        Robot robot = new Robot("Robot", 'R', Robot.Min_X, 0);
        assertEquals(false, robot.left());
        assertEquals(Robot.Min_X, robot.getX());
    }

    @Test
    public void shouldrightSuccess() {
        Robot robot = new Robot("Robot", 'R', 0, 0);
        assertEquals(true, robot.right());
        assertEquals(1, robot.getX());
    }

    @Test
    public void shouldleftNegative() {
        Robot robot = new Robot("Robot", 'R', Robot.Max_X, 0);
        assertEquals(false, robot.right());
        assertEquals(Robot.Max_X, robot.getX());
    }

    @Test
    public void shouldleftSuccess() {
        Robot robot = new Robot("Robot", 'R', 1, 0);
        assertEquals(true, robot.left());
        assertEquals(0, robot.getX());
    }
}
